# Сквозное логирование для микросервисов

### Требования
PHP 5.4+ с модулями bcmath, zip

## Установка
Добавьте в composer.json:
```
"mkrutikov/microservice_logging": "~1.0"
```

## Доработка пакета
Если необходимо доработать пакет, установите зависимости.

### Установка зависимостей
Для разработки пакета необходимо установить зависимости.
Сделать это с помощью Докера можно командами (все команды запускать в папке с проектом):
```
docker-compose build
docker-compose up -d
docker exec php composer install
```
Остановка запущенного Докер-контейнера:
```
docker-compose down
```

### Запуск тестов
Для запуска unit тестов проекта вызовите (на хост-машине) команду
```
docker exec php vendor/bin/phpunit tests
```
Если разработка ведется не в докере, то тесты запускаются так:
```
vendor/bin/phpunit tests
```

## Примеры использования
В клиентском коде необходимо добавить логирование в RabbitMQ
```php
$connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
$amqpHandlerFactory = new RabbitLogHandlerFactory($connection);
$amqpHandler = $this->amqpHandlerFactory->createHandler('logging', 'log');

$logger = new Logger('logger_name');
$logger->pushHandler($amqpHandler);

// Теперь можно писать в лог сообщения
$logContext = [];
$logContext['guid'] = '555';
$messageBody = 'Текст сообщения';
$logger->debug($messageBody, $this->logContext);
```
За документацией по Monolog обращайтесь к официальному репозиторию: https://github.com/Seldaek/monolog

### Генерация GUID: класс GuidGenerator
Для генерации идентификатора, который передается в запросах между микросервисам, используется класс GuidGenerator
При инициализации объекта необходимо передать в конструктор массив, аналогичный суперглобальному массиву $_SERVER 
(или саму переменную $_SERVER). Класс попытается найти в этом массиве значение по ключу HTTP_X_CORRELATION_ID, 
которое соответствует заголовку X-Correlation-Id 
(служит для передачи GUID в запросах). Если заголовок не найден, генерируется новый идентификатор.
```php
$guidGenerator = new GuidGenerator($_SERVER);
$guid = $guidGenerator->getGuid();
```
Метод getGuid() возвращает идентификатор, который можно использовать при логировании и вызовах других сервисов.

### Класс MicroserviceClient для вызовов микросервисов
Класс является оберткой библиотеки Guzzle. Необходимый параметр конструктора - идентификатор GUID, который будет 
передан в заголовке запроса.
```php
$microserviceClient = new MicroserviceClient($guid);
$microserviceResponse = $microserviceClient->request('GET', 'http://city_service.m.krutikov.docker:8080/index.php');
echo $microserviceResponse->getBody();
```