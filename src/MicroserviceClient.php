<?php

namespace MicroserviceLogging;

use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;

/**
 * Класс для отправки HTTP запросов микросервияам. По сути, обертка Guzzle, добавляющая заголовок с GUID
 * @package MicroserviceLogging
 * TODO После перехода всех сервисов на PHP 5.5+ перейти на версию Guzzle 6. Там можно делать моки соединений.
 */
class MicroserviceClient
{
    /** @var array */
    private $headers;

    /** @var Client */
    private $guzzleClient;

    /**
     * @param string $guid
     * @param array $guzzleConfig
     */
    public function __construct($guid, $guzzleConfig = [])
    {
        $this->headers = [
            GuidGenerator::GUID_HEADER_NAME => $guid,
        ];

        $this->guzzleClient = new Client($guzzleConfig);
    }

    /**
     * Делает запрос и возвращает ответ
     *
     * @param string $method Метод (GET, POST, etc)
     * @param string $url
     * @param array $headers Заголовки запроса
     * @param mixed|null $body Тело запроса
     * @return \GuzzleHttp\Message\FutureResponse|\GuzzleHttp\Message\ResponseInterface|\GuzzleHttp\Ring\Future\FutureInterface|null
     */
    public function request($method, $url, $headers = [], $body = null)
    {
        $headers = array_merge($headers, $this->headers);

        $request = new Request($method, $url, $headers, $body);
        $response = $this->guzzleClient->send($request);

        return $response;
    }
}
