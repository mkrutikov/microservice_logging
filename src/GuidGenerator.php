<?php

namespace MicroserviceLogging;

/**
 * Класс отвечает за генерацию сквозного идентификатора (GUID)
 * @package MicroserviceLogging
 */
class GuidGenerator
{
    // имя HTTP-заголовка для передачи GUID
    const GUID_HEADER_NAME = 'X-Correlation-Id';

    /**
     * @var array Массив, содержащийся в переменной $_SERVER. Заголовки запроса начинаются с HTTP_
     */
    private $rawHeaders;

    public function __construct($rawHeaders)
    {
        $this->rawHeaders = $rawHeaders;
    }

    /**
     * Возвращает GUID, полученный из заголовка или сгенерированный, если в заголовке он не передан
     * @return string|null $guid
     */
    public function getGuid()
    {
        $guid = $this->getHeaderValue(self::GUID_HEADER_NAME);
        if (is_null($guid)) {
            $guid = uniqid('', true);
        }
        return $guid;
    }

    /**
     * Получает массив HTTP-заголовков запроса
     * @return array|false
     */
    private function getHeaders()
    {
        $headers = [];
        foreach ($this->rawHeaders as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }

    /**
     * Получает значение HTTP заголовка по имени
     * @param string $headerName
     * @return mixed|null
     */
    private function getHeaderValue($headerName)
    {
        foreach ($this->getHeaders() as $name => $value) {
            if ($name === $headerName) {
                return $value;
            }
        }
        return null;
    }
}
