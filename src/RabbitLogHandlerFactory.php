<?php

namespace MicroserviceLogging;

use Monolog\Handler\AmqpHandler;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Класс для создания объектов обработчика Monolog, который отправляет логи в RabbitMQ
 * Class RabbitLogHandlerFactory
 * @package MicroserviceLogging
 */
class RabbitLogHandlerFactory
{
    /** @var AmqpHandler */
    private $handler;

    /** @var AMQPStreamConnection */
    private $amqpConnection;

    /** @var AMQPChannel  */
    private $amqpChannel;

    private $passive = false;
    private $durable = true;

    /**
     * Инициализация канала RabbitMQ
     * @param AMQPStreamConnection $amqpConnection
     */
    public function __construct(AMQPStreamConnection $amqpConnection)
    {
        $this->amqpConnection = $amqpConnection;
        $this->amqpChannel = $this->amqpConnection->channel();
    }

    /**
     * @param bool $passive
     * @return RabbitLogHandlerFactory
     */
    public function setPassive($passive)
    {
        $this->passive = $passive;
        return $this;
    }

    /**
     * @param bool $durable
     * @return RabbitLogHandlerFactory
     */
    public function setDurable($durable)
    {
        $this->durable = $durable;
        return $this;
    }

    /**
     * Создает и возвращает обработчик для Monolog
     * @param string $queueName
     * @param string $exchangeName
     * @return AmqpHandler
     */
    public function createHandler($queueName, $exchangeName)
    {
        $this->amqpChannel->queue_declare($queueName, $this->passive, $this->durable, false, false);
        $this->amqpChannel->exchange_declare($exchangeName, 'fanout', $this->passive, $this->durable, false);
        $this->amqpChannel->queue_bind($queueName, $exchangeName);

        $this->handler = new AmqpHandler($this->amqpChannel, $exchangeName);

        return $this->handler;
    }

    public function __destruct()
    {
        $this->closeConnection();
    }

    /**
     * Закрывает соединение с RabbitMQ
     */
    public function closeConnection()
    {
        $this->amqpChannel->close();
    }
}
