<?php

use MicroserviceLogging\RabbitLogHandlerFactory;
use PHPUnit\Framework\TestCase;

class RabbitLogHandlerFactoryTest extends TestCase
{
    /**
     * TODO при переходе всех проектов на PHP 5.6+ переделать получение имен классов на ClassName::class
     */
    public function testConstructor()
    {
        $rabbitConnectionStub = $this->getMockBuilder('PhpAmqpLib\Connection\AMQPStreamConnection')
            ->disableOriginalConstructor()
            ->getMock();

        $rabbitChannelStub = $this->getMockBuilder("PhpAmqpLib\Channel\AMQPChannel")
            ->disableOriginalConstructor()
            ->getMock();

        $rabbitConnectionStub->method('channel')->willReturn($rabbitChannelStub);
        $rabbitConnectionStub->expects($this->once())->method('channel');

        $factory = new RabbitLogHandlerFactory($rabbitConnectionStub);
    }

    public function testCreateHandler()
    {
        $rabbitConnectionStub = $this->getMockBuilder('PhpAmqpLib\Connection\AMQPStreamConnection')
            ->disableOriginalConstructor()
            ->getMock();

        $rabbitChannelStub = $this->getMockBuilder("PhpAmqpLib\Channel\AMQPChannel")
            ->disableOriginalConstructor()
            ->getMock();
        $rabbitConnectionStub->method('channel')->willReturn($rabbitChannelStub);

        $factory = new RabbitLogHandlerFactory($rabbitConnectionStub);
        $queueName = 'queue';
        $exchangeName = 'exchange';

        $this->assertInstanceOf("Monolog\Handler\AmqpHandler", $factory->createHandler($queueName, $exchangeName));
    }
}
