<?php

use MicroserviceLogging\GuidGenerator;
use PHPUnit\Framework\TestCase;

class GuidGeneratorTest extends TestCase
{
    /**
     * @dataProvider providerTestGetGuid
     * @param array $rawHeaders
     * @param string|null $expectedGuid
     */
    public function testGetGuid($rawHeaders, $expectedGuid)
    {
        $guidGenerator = new GuidGenerator($rawHeaders);
        $guid = $guidGenerator->getGuid();

        if (!is_null($expectedGuid)) {
            $this->assertEquals($expectedGuid, $guid);
        }

        $this->assertNotNull($guid);
    }

    /**
     * @return array
     */
    public function providerTestGetGuid()
    {
        return [
            'empty_headers' => [
                'rawHeaders' => [],
                'expectedGuid' => null,
            ],
            'correct_headers' => [
                'rawHeaders' => [
                    'Cache-Control' => 'no-cache',
                    'REMOTEPORT' => '9999',
                    'HTTPREFERER' => 'http://previous.url.com',
                    'HTTP_MY_HEADER' => 'SomeValue',
                    'HTTP_X_CORRELATION_ID' => 'my-id',
                ],
                'expectedGuid' => 'my-id',
            ],
            'no_correlation_id' => [
                'rawHeaders' => [
                    'Cache-Control' => 'no-cache',
                    'HTTPREFERER' => 'http://previous.url.com',
                    'HTTP_MY_HEADER' => 'SomeValue',
                ],
                'expectedGuid' => null,
            ],
        ];
    }


    /**
     * @dataProvider providerTestGetHeaders
     * @param array $rawHeaders
     * @param int $expectedHeadersCount
     * @throws ReflectionException
     */
    public function testGetHeaders($rawHeaders, $expectedHeadersCount)
    {
        $guidGenerator = new GuidGenerator($rawHeaders);

        $getHeadersMethod = new ReflectionMethod(get_class($guidGenerator), 'getHeaders');
        $getHeadersMethod->setAccessible(true);

        $this->assertEquals($expectedHeadersCount, sizeof($getHeadersMethod->invoke($guidGenerator, $rawHeaders)));
    }

    /**
     * @return array
     */
    public function providerTestGetHeaders()
    {
        return [
            'empty_headers' => [
                'rawHeaders' => [],
                'expectedHeadersCount' => 0,
            ],
            'correct_headers' => [
                'rawHeaders' => [
                    'Cache-Control' => 'no-cache',
                    'REMOTEPORT' => '9999',
                    'HTTPREFERER' => 'http://previous.url.com',
                    'HTTP_MY_HEADER' => 'SomeValue',
                    'HTTP_X_CORRELATION_ID' => 'my-id',
                ],
                'expectedHeadersCount' => 2,
            ],
        ];
    }

    /**
     * @dataProvider providerTestGetHeaderValue
     * @param array $rawHeaders
     * @param string $headerName
     * @param mixed|null $expectedValue
     * @throws ReflectionException
     */
    public function testGetHeaderValue($rawHeaders, $headerName, $expectedValue)
    {
        $guidGenerator = new GuidGenerator($rawHeaders);

        $getHeaderValueMethod = new ReflectionMethod(get_class($guidGenerator), 'getHeaderValue');
        $getHeaderValueMethod->setAccessible(true);

        $this->assertEquals($expectedValue, $getHeaderValueMethod->invoke($guidGenerator, $headerName));
    }

    /**
     * @return array
     */
    public function providerTestGetHeaderValue()
    {
        return [
            'empty_headers' => [
                'rawHeaders' => [],
                'headerName' => 'My-Header',
                'expectedValue' => null,
            ],
            'header_not_found' => [
                'rawHeaders' => [
                    'HTTP_X_CORRELATION_ID' => 'my-id',
                ],
                'headerName' => 'My-Header',
                'expectedValue' => null,
            ],
            'header_found' => [
                'rawHeaders' => [
                    'Cache-Control' => 'no-cache',
                    'REMOTEPORT' => '9999',
                    'HTTPREFERER' => 'http://previous.url.com',
                    'HTTP_MY_HEADER' => 'SomeValue',
                    'HTTP_X_CORRELATION_ID' => 'my-id',
                ],
                'headerName' => 'My-Header',
                'expectedValue' => 'SomeValue',
            ],
        ];
    }
}
