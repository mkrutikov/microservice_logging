FROM php:5.4
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY . /usr/app
WORKDIR /usr/app
RUN apt-get update && apt-get install -y git curl zlib1g-dev bash unzip \
&& docker-php-ext-install bcmath zip \
&& docker-php-ext-enable bcmath zip

CMD [ "php"]
EXPOSE 22